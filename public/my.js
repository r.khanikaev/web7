          $(document).ready(function(){
              $('.slider').slick({
                  dots:true,
                  appendArrows:'.slider-arrows',
                  prevArrow:'<span class="slider-arrow1"><</span>',
                  nextArrow:'<span class="slider-arrow2">></span>',
                  infinite: true,
                  speed: 300,
                  slidesToShow: 4,
                  slidesToScroll: 4,
                  responsive: [
                      {
                          breakpoint: 1024,
                          settings: {
                              slidesToShow: 2,
                              slidesToScroll: 2,
                              infinite: true,
                              dots: true
                          }
                      },
                      {
                          breakpoint: 600,
                          settings: {
                              slidesToShow: 2,
                              slidesToScroll: 2,
                              infinite: true,
                              dots: true
                          }
                      },
                      {
                          breakpoint: 480,
                          settings: {
                              slidesToShow: 2,
                              slidesToScroll: 2,
                              infinite: true,
                              dots: true
                          }
                      }
                  ]
              });
          });